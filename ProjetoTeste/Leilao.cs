﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjetoTeste
{
    public class Leilao
    {
        public string Descricao { get; set; }
        public List<Lance> Lances { get; set; }

        public Leilao(string descricao)
        {
            this.Descricao = descricao;
            this.Lances = new List<Lance>();
        }

        public void Propoe(Lance lance)
        {
            

            if (Lances.Count == 0 || 
                PodeDarLance(lance.Usuario))
            {
                Lances.Add(lance);
            }
            
        }

        private bool PodeDarLance(Usuario usuario)
        {
            return (!UltimoLanceDado().Usuario.Equals(usuario) && QtdeLancesDo(usuario) < 5);
        }

        private int QtdeLancesDo(Usuario usuario)
        {
            int total = 0;

            foreach (var l in Lances)
            {
                if (l.Usuario.Equals(usuario)) total++;
            }
            return total;
        }

        private Lance UltimoLanceDado()
        {
            return Lances[Lances.Count - 1];
        }
    }
}
