﻿using System;

namespace ProjetoTeste
{
    public class Lance
    {
        public Usuario Usuario { get; set; }
        public double Valor { get; private set; }
        
        public Lance(Usuario usuario, double valor)
        {
            this.Usuario = usuario;
            this.Valor = valor;
        }
    }
}
