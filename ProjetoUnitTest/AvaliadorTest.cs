﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ProjetoTeste
{
    [TestClass]
    public class AvaliadorTest
    {
        private Avaliador leiloeiro;
        private Usuario joao;
        private Usuario maria;
        private Usuario jose;

        [TestInitialize]
        public void CriaAvaliador()
        {
            this.leiloeiro = new Avaliador();
            this.joao = new Usuario("Joao");
            this.jose = new Usuario("Jose");
            this.maria = new Usuario("Maria");
        }

        [TestMethod]
        public void DeveEntenderLancesEmOrderCrescente()
        {
            //1 parte - Cenario

            Leilao leilao = new CriadorDeLeilao()
                .Para("Playstation 03 novo")
                .Lance(maria, 250.00)
                .Lance(joao, 300.00)
                .Lance(jose, 400.00)
                .Constroi();

            //2 parte - Acao

            leiloeiro.Avalia(leilao);

            //3 parte - Validacao

            Assert.AreEqual(leiloeiro.MaiorLance, 400, 0.0001);
            Assert.AreEqual(leiloeiro.MenorLance, 250, 0.0001);

        }

        [TestMethod]
        public void DeveEntenderLeilaoComApenasUmLance()
        {
            //1 parte - Cenario

            Leilao leilao = new CriadorDeLeilao()
                .Para("Playstation 03 novo")
                .Lance(joao, 250)
                .Constroi();

            //2 parte - Acao

            leiloeiro.Avalia(leilao);

            //3 parte - Validacao

            Assert.AreEqual(leiloeiro.MaiorLance, 250, 0.0001);
            Assert.AreEqual(leiloeiro.MenorLance, 250, 0.0001);
        }
        
        [TestMethod]
        public void DeveEncontrarOsTresMaioresLances()
        {
            //1 parte - Cenario

            Leilao leilao = new CriadorDeLeilao()
                .Para("Playstation 03 novo")
                .Lance(joao, 100)
                .Lance(maria, 200)
                .Lance(joao, 300)
                .Lance(maria, 400)
                .Constroi();

            //2 parte - Acao

            leiloeiro.Avalia(leilao);

            //3 parte - Validacao

            var maiores = leiloeiro.TresMaiores;

            Assert.AreEqual(3, maiores.Count);
            Assert.AreEqual(400, maiores[0].Valor, 0.0001);
            Assert.AreEqual(300, maiores[1].Valor, 0.0001);
            Assert.AreEqual(200, maiores[2].Valor, 0.0001);

        }
    }
}

// Classes de equivalência
// Escrever um teste por classe de equivalencia
// Desafio: Encontrar diferentes classes de equivalencia

// Testar Listas
// Testar o tamanho 
// Testar o conteudo

//Classes de equivalencia que devem ser testadas

//Ordem decrescente
//Ordem randomica

// DELTA - Assert encima de um double

// Padrao de Projeto
// Test Data Builder
// 
// 
// 
// 