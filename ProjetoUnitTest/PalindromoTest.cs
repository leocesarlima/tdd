﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ProjetoTeste
{
    [TestClass]
    public class PalindromoTest
    {
        
        [TestMethod]
        public void DeveIdentificarPalindromoEFiltrarCaracteresInvalidos()
        {
            Palindromo p = new Palindromo();

            bool resultado = p.EhPalindromo("Socorram-me subi no onibus em Marrocos");

            Assert.IsTrue(resultado);
        }

        [TestMethod]
        public void DeveIdentificarPalindromo()
        {
            Palindromo p = new Palindromo();
            bool resultado = p.EhPalindromo("Anotaram a data da maratona");
            Assert.IsTrue(resultado);
        }

        [TestMethod]
        public void DeveIdentificarSeNaoEhPalindromo()
        {
            Palindromo p = new Palindromo();

            bool resultado = p.EhPalindromo("E preciso amar as pessoas como se nao houvesse amanha");
            Assert.IsFalse(resultado);
        }
    }
}
