﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ProjetoTeste
{
    [TestClass]
    public class LeilaoTest
    {
        [TestMethod]
        public void DeveReceberUmLance()
        {
            //1 parte - Cenario
            Usuario joao = new Usuario("Joao");

            Leilao leilao = new Leilao("Playstation 03 novo");
            Assert.AreEqual(0, leilao.Lances.Count);

            leilao.Propoe(new Lance(joao, 1000.00));
            Assert.AreEqual(1, leilao.Lances.Count);
            Assert.AreEqual(1000, leilao.Lances[0].Valor, 0.0001);
        }

        [TestMethod]
        public void DeveReceberVariosLances()
        {
            Usuario joao = new Usuario("Joao");
            Usuario maria = new Usuario("Maria");

            Leilao leilao = new Leilao("Playstation 03 novo");

            leilao.Propoe(new Lance(joao, 1000.00));
            leilao.Propoe(new Lance(maria, 2000.00));

            Assert.AreEqual(2, leilao.Lances.Count);
            Assert.AreEqual(1000, leilao.Lances[0].Valor, 0.0001);
            Assert.AreEqual(2000, leilao.Lances[1].Valor, 0.0001);
        }

        [TestMethod]
        public void NaoDeveAceitarDoisLancesSeguidosDoMesmoUsuario()
        {
            Usuario joao = new Usuario("Joao");
            Leilao leilao = new Leilao("Playstation 03 novo");

            leilao.Propoe(new Lance(joao, 1000.00));
            leilao.Propoe(new Lance(joao, 2000.00));

            Assert.AreEqual(1, leilao.Lances.Count);
            Assert.AreEqual(1000, leilao.Lances[0].Valor, 0.0001);
        }
        
        [TestMethod]
        public void NaoDeveAceitarMaisDoQueCincoLancesDeUmMesmoUsuario()
        {
            Usuario joao = new Usuario("Joao");
            Usuario maria = new Usuario("Maria");

            Leilao leilao = new Leilao("Playstation 03 novo");

            leilao.Propoe(new Lance(joao, 1000.00));
            leilao.Propoe(new Lance(maria, 2000.00));

            leilao.Propoe(new Lance(joao, 3000.00));
            leilao.Propoe(new Lance(maria, 4000.00));

            leilao.Propoe(new Lance(joao, 5000.00));
            leilao.Propoe(new Lance(maria, 6000.00));

            leilao.Propoe(new Lance(joao, 7000.00));
            leilao.Propoe(new Lance(maria, 8000.00));

            leilao.Propoe(new Lance(joao, 9000.00));
            leilao.Propoe(new Lance(maria, 10000.00));

            leilao.Propoe(new Lance(joao, 11000.00));

            Assert.AreEqual(10, leilao.Lances.Count);
            Assert.AreEqual(10000, leilao.Lances[leilao.Lances.Count -1].Valor, 0.0001);
        }
    }
}

// TDD - Test Driven Development (Começar pelo teste)
// Desenvolvimento Guiado Pelos Testes
// Escreve o teste da maneira mais simples
// Falha - Passa - Refatora
// 
// 